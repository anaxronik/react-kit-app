import { FormStore } from "./index";

const defaultState = {
  string: "string",
  number: 0,
  bool: false,
};

describe("FormStore test", () => {
  const form = new FormStore(defaultState);

  test("Форма создается", () => {
    expect(form).toBeDefined();
    expect(form).not.toBeUndefined();
  });

  test("Данные в форме инициализируются", () => {
    expect(form.values).toEqual(defaultState);
  });

  test("FormStore.changeValue", () => {
    form.changeValue("STRING", "string");
    expect(form.values.string).toEqual("STRING");
    form.changeValue(1, "number");
    expect(form.values.number).toEqual(1);
    form.changeValue(true, "bool");
    expect(form.values.bool).toEqual(true);
  });

  test("FormStore.reset(key)", () => {
    form.reset("string");
    expect(form.values.string).toBeNull();
    form.reset("number");
    expect(form.values.number).toBeNull();
    form.reset("bool");
    expect(form.values.bool).toBeNull();
  });

  test("FormStore.reset()", () => {
    form.changeValueByKey("STRING", "string");
    form.changeValue(1, "number");
    form.changeValue(true, "bool");
    expect(form.values.string).toEqual("STRING");
    expect(form.values.number).toEqual(1);
    expect(form.values.bool).toEqual(true);

    form.reset();

    expect(form.values.string).toBeNull();
    expect(form.values.number).toBeNull();
    expect(form.values.bool).toBeNull();
  });
});

describe("Form store errors without schema", () => {
  const store = new FormStore(defaultState);
  test("без схемы в ошибке должны быть null", () => {
    expect(store.errors.string).toBeNull();
    expect(store.errors.number).toBeNull();
    expect(store.errors.bool).toBeNull();
  });
});
