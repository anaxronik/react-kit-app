import * as yup from "yup";

export const testSchema = yup.object({
  firstName: yup
    .string()
    .typeError("Неверный тип данных")
    .required("Обязательно к заполнению"),
  lastName: yup
    .string()
    .typeError("Неверный тип данных")
    .required("Обязательно к заполнению"),
  surName: yup
    .string()
    .typeError("Неверный тип данных")
    .required("Обязательно к заполнению"),
});
