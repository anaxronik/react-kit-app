import { makeAutoObservable, runInAction } from "mobx";
import { SchemaOf } from "yup";

type TValue = string | number | boolean | null;
export class FormStore<Values extends object> {
  private _values: Values;
  private _errors: { [key in keyof Values]: string | null };
  private validationSchema:
    | undefined
    | SchemaOf<{ [key in keyof Values]: any }>;

  constructor(
    values: Values,
    validationSchema?: SchemaOf<{ [key in keyof Values]: any }>
  ) {
    this._values = values;

    const errors = JSON.parse(JSON.stringify(values));
    Object.keys(errors).forEach((key) => {
      errors[key] = null;
    });
    this._errors = errors;

    if (validationSchema) this.validationSchema = validationSchema;
    makeAutoObservable(this);
  }

  /** Изменяет значение value по key
   * - Если в констурктор передана валидационная схема заодно и валидацию запускает
   */
  changeValue = (value: TValue, key?: string) => {
    if (key) this.values = { ...this.values, [key]: value };
    if (this.validationSchema) this.validate(key as keyof Values);
  };

  changeValueByKey = (value: TValue, key?: keyof Values) =>
    this.changeValue(value, key as string);

  /** присваивает всем values и errors null
   * - Если передать key то сбросит только одно значение
   * -  Если ничего не передавать то сбросит все
   */
  reset = (key?: keyof Values) => {
    this.resetValues();
    this.resetError();
  };

  private changeError = (value: string | null, key: keyof Values) => {
    this._errors = { ...this._errors, [key]: value };
  };

  /** Валидация value по схеме
   * - Если передать key, то провалидруется только одно поле
   * - Если не передавать, то все поля values
   */
  validate = (key?: keyof Values) => {
    if (this.validationSchema) {
      if (key) {
        this.validationSchema
          .validateAt(String(key), this.values)
          .then(() => {
            this.changeError(null, key);
          })
          .catch((err: { errors: string[] }) => {
            this.changeError(err?.errors?.join(" ") || "Неверно", key);
          });
      } else {
        Object.keys(this.values).forEach((key) => {
          this.validate(key as keyof Values);
        });
      }
    }
  };

  /** Сброс values на null
   * - Если передать key, то только одно поле key
   * - Если не передавать key, то все поля values
   */
  resetValues = (key?: keyof Values) => {
    if (key) {
      this._values = { ...this._values, [key]: null };
    } else {
      Object.keys(this._values).forEach((valuesKey) => {
        this._values = { ...this._values, [valuesKey]: null };
      });
    }
  };

  /** Сброс ошибок на null
   * - Если передать key, то только одно поле key
   * - Если не передавать key, то все поля errors
   */
  resetError = (key?: keyof Values) => {
    if (key) {
      this._errors = { ...this._errors, [key]: null };
    } else {
      Object.keys(this._errors).forEach((errorKey) => {
        this._errors = { ...this._errors, [errorKey]: null };
      });
    }
  };

  /** true/false Все поля values соответствуют валидацаионной схеме */
  get isValid(): boolean {
    if (this.validationSchema) {
      return this.validationSchema.isValidSync(this.values);
    } else {
      return true;
    }
  }

  get values() {
    return this._values;
  }

  set values(values: Values) {
    runInAction(() => {
      this._values = values;
    });
  }

  get errors() {
    return this._errors;
  }

  log = () => {
    console.log(
      JSON.parse(
        JSON.stringify({
          errors: this._errors,
          values: this._values,
        })
      )
    );
  };

  get adapters() {
    let adapters: {
      [key in keyof Values]: {
        value: string | null;
        errorText: string | null;
        name: string;
        onChange: (value: any, key: any) => void;
      };
    } = {} as any;
    Object.keys(this._values).forEach((key) => {
      adapters = {
        ...adapters,
        [key]: {
          value: this.values[key as keyof Values],
          onChange: this.changeValue,
          name: key,
          errorMsg: this.errors[key as keyof Values],
        },
      };
    });
    return adapters;
  }
}

export default FormStore;
