import { FocusEvent, ReactElement } from "react";

export interface IInputProps<Name> {
  value: string | null;
  type?:
    | "button"
    | "checkbox"
    | "color"
    | "date"
    | "datetime-local"
    | "email"
    | "file"
    | "hidden"
    | "image"
    | "month"
    | "number"
    | "password"
    | "radio"
    | "range"
    | "reset"
    | "search"
    | "submit"
    | "tel"
    | "text"
    | "time"
    | "url"
    | "week";
  onChange: (value: string, name?: Name) => void;
  onBlur?: (event: FocusEvent<HTMLInputElement>) => void;
  errorMsg?: string | null;
  placeholder?: string;
  iconRight?: ReactElement;
  iconLeft?: ReactElement;
  id?: string;
  name?: Name;
  mask?: string;
  disabled?: boolean;
  withSearch?: boolean;
  variants?: string[];
  formatChars?: any;
  className?: string;
}
