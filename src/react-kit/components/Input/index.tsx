import { observer } from "mobx-react";
import React from "react";
import "./style.scss";

interface IProps {
  value: string | number | null;
  onChange: (value: string, name?: string) => void;
  name?: string;
  className?: string;
  disabled?: boolean;
  placeholder?: string;
  label?: string;
  errorMsg?: string | null;
}

const Input: React.FC<IProps> = (props) => {
  return (
    <div className={`input ${props.className}`}>
      {props.label && !!props.value && (
        <div className="input__label">{props.label}</div>
      )}
      <input
        {...props}
        className={[`input-input`, props.errorMsg && "input-input--error"].join(
          " "
        )}
        value={props.value || ""}
        onChange={(e) => props.onChange(e.target.value, props.name)}
      />
    </div>
  );
};

export default observer(Input);
