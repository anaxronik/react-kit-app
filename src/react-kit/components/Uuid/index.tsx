import React from "react";
import { v4 as uuidv4 } from "uuid";

const Uuid = () => {
  return <div>{uuidv4()}</div>;
};

export default Uuid;
