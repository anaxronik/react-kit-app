import { observer } from "mobx-react";
import React from "react";
import Input from "./react-kit/components/Input";
import { FormStore } from "./react-kit/stores/FormStore";
import { testSchema } from "./react-kit/stores/FormStore/testValidationSchema";

const form = new FormStore(
  {
    firstName: null,
    lastName: "null",
    surName: null,
  },
  testSchema
);

function App() {
  return (
    <div className="container">
      <div>asdasd</div>
      <Input
        {...form.adapters.firstName}
        placeholder={"dqwdqwdqwd"}
        label={"label text"}
      />
      <Input
        {...form.adapters.lastName}
        placeholder={"dqwdqwdqwd"}
        label={"label text"}
      />
      <Input
        {...form.adapters.surName}
        placeholder={"dqwdqwdqwd"}
        label={"label text"}
      />
      <Input {...form.adapters.lastName} disabled />
    </div>
  );
}

export default observer(App);
